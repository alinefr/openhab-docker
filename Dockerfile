ARG TAG=latest

FROM openhab/openhab:$TAG

ARG DEBIAN_FRONTEND=noninteractive

RUN apt-get update \
    && apt-get install -y \
      python3-pip \
      ffmpeg \
    && pip3 install speedtest-cli \
    && apt-get clean \
    && rm -rf /var/lib/apt/lists/*
